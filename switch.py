import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
import time

class Switch:
    def __init__(self):
        GPIO.setmode(GPIO.BCM) # Use physical pin numbering
        GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
        GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def is_right(self):
        while True:
            is_right = False
            if not GPIO.input(18):
                is_right = True
                time.sleep(0.2)
                break

            if not GPIO.input(23):
                time.sleep(0.2)
                break

        return is_right
