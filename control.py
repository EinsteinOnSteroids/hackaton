import serial

#class to contol servo -> arduino
class Control:

    def __init__(self):
        ''' Configurate UART'''
        self.port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=3.0)


    '''Hands'''

    def hand_r(self, mode):
        msg = "error"
        if mode == "up":
            msg = 100
        elif mode == "center":
            msg = 101
        elif mode == "down":
            msg = 102

        #send command
        if msg == "error":
            print(msg)
        else:
            self.port.write([msg])


    def hand_l(self, mode):
        msg = "error"
        if mode == "up":
            msg = 110
        elif mode == "center":
            msg = 111
        elif mode == "down":
            msg = 112

        #send command
        if msg == "error":
            print(msg)
        else:
            self.port.write([msg])


    '''Ears'''

    def ear_r(self, mode):
        msg = "error"
        if mode == "forward":
            msg = 200
        elif mode == "center":
            msg = 201
        elif mode == "back":
            msg = 202

        #send command
        if msg == "error":
            print(msg)
        else:
            self.port.write([msg])


    def ear_l(self, mode):
        msg = "error"
        if mode == "forward":
            msg = 211
        elif mode == "center":
            msg = 212
        elif mode == "back":
            msg = 213

        #send command
        if msg == "error":
            print(msg)
        else:
            self.port.write([msg])

    def hight_five(self):
        self.port.write("150")
