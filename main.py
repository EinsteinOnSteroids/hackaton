#!/usr/bin/env python

#import packages
import time
import random
#import self
import control
import speaker
import switch


class Sequence:
    def __init__(self, audio, send):
        self.audio = audio
        self.send = send

    def to_center(self):
        self.send.hand_r("center")
        self.send.hand_l("center")
        self.send.ear_r("center")
        self.send.ear_l("center")

    def seq1(self):
        while self.audio.is_playing():
            self.send.hand_r("up")
            time.sleep(1)
            self.send.ear_r("forward")
            self.send.ear_l("back")
            time.sleep(1)
            self.send.hand_r("center")
            self.send.ear_r("back")
            self.send.ear_l("back")
            self.send.hand_l("down")
            time.sleep(1)
            self.to_center()

    def seq_dj(self):
            self.send.ear_r("back")
            self.send.ear_l("back")
            self.send.hand_r("up")
            time.sleep(4)
            for i in range(3):
                self.send.hand_r("down")
                self.send.ear_r("forward")
                self.send.ear_l("forward")
                time.sleep(0.5)
                self.send.hand_r("up")
                self.send.ear_r("back")
                self.send.ear_l("back")
                self.sleep(0.5)

            self.to_center()
            while self.audio.is_playing():
                continue


if __name__ == "__main__":
    #initialize
    send = control.Control()
    audio = speaker.Speaker()
    button = switch.Switch()
    sequence = Sequence(audio, send)

    #when mis start
    send.ear_r("forward")
    send.ear_l("forward")
    send.ear_r("center")
    send.ear_l("center")
    send.ear_r("back")
    send.ear_l("back")
    send.ear_r("center")
    send.ear_l("center")

    #introduce yourself
    audio.play("audio/intro.mp3")

    while True:
        which = random.randint(1,5)

        #jokes
        if which == 1:
            audio.play("audio/tell_joke.mp3")
            send.hand_r("up")
            send.hand_l("down")
            time.sleep(0.4)
            send.hand_r("center")
            send.hand_l("center")
            if button.is_right():
                which_joke = random.randint(1, 5)
                if which_joke == 1:
                    audio.play("audio/joke1.mp3")
                if which_joke == 2:
                    audio.play("audio/joke2.mp3")
                if which_joke == 3:
                    audio.play("audio/joke3.mp3")
                if which_joke == 4:
                    audio.play("audio/joke4.mp3")
                if which_joke == 5:
                    audio.play("audio/joke5.mp3")

                sequence.seq1()

            else:
                audio.play("audio/something_else.mp3")
                sequence.seq1()

        #DJ
        if which == 2:
            audio.play("audio/was_dj.mp3")
            sequence.seq1()
            audio.play("audio/dj.mp3")
            sequence.seq_dj()

        #riddle
        if which == 3:
            audio.play("audio/tell_riddle.mp3")
            sequence.seq1()
            which_riddle = random.randint(1, 4)
            if which_riddle == 1:
                audio.play("audio/riddle1.mp3")
                if button.is_right():
                    audio.play("audio/riddle1_ans.mp3")
            if which_riddle == 2:
                audio.play("audio/riddle2.mp3")
                if button.is_right():
                    audio.play("audio/riddle2_ans.mp3")
            if which_riddle == 3:
                audio.play("audio/riddle3.mp3")
                if button.is_right():
                    audio.play("audio/riddle3_ans.mp3")
            if which_riddle == 4:
                audio.play("audio/riddle4.mp3")
                if button.is_right():
                    audio.play("audio/riddle4_ans.mp3")

            audio.play("audio/right_answer.mp3")
            sequence.seq1()
            if button.is_right():
                audio.play("audio/good_ans.mp3")
            else:
                audio.play("audio/no_worry.mp3")
            sequence.seq1()

        #random high five
        if which == 4:
            audio.play("audio/random_hv.mp3")
            while audio.is_playing():
                continue

        #game
        if which == 5:
            sequence.to_center()
            audio.play("audio/game.mp3")
            while audio.is_playing():
                continue

            points = 0
            correct = False
            for i in range(7):
                t = random.random(2)

                which_leg = random.randint(1,2)

                if which_leg == 1:
                    audio.play("audio/right.mp3")
                else:
                    audio.play("audio/left.mp3")
                while audio.is_playing():
                    continue

                t_start = time.now()
                while (t_start - time.now) < 0.3:
                    if button.is_right and which_leg == 1:
                        correct = True
                        dt = t_start - time.now()
                    elif not button.is_right and which_leg == 2:
                        correct = True

                if correct:
                    correct = False
                    points += 1

                time.sleep(t)

            if points > 4:
                audio.play("audio/win.mp3")
            else:
                audio.play("audio/lose.mp3")
            while audio.is_playing():
                continue
